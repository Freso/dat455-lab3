#!/usr/bin/env python3
"""Linear regression implemented in pure Python."""

import sys

import matplotlib.pyplot as plt

from matrix import *


def compute_linear_regression(x: list, y: list) -> list:
    """Compute linear regression matrix for x, y."""
    x_p = powers(x, 0, 1)
    y_p = powers(y, 1, 1)
    x_pt = transpose(x_p)
    return matmul(invert(matmul(x_pt, x_p)), matmul(x_pt, y_p))


def predict_outcome(model: list, value: float) -> float:
    """Return a prediction for an outcome for a value given a model.

    >>> predict_outcome([[10.0], [20.0]], 5)
    110.0
    """
    return model[0][0] + model[1][0] * value


def main(argv):
    """Run regression on data from given file and print analysis."""
    data_points = loadtxt(argv[1])
    x_vals = [d[0] for d in data_points]
    y_vals = [d[1] for d in data_points]
    model = compute_linear_regression(x_vals, y_vals)

    plt.plot(x_vals, y_vals, 'ro')
    plt.plot(x_vals, [predict_outcome(model, x) for x in x_vals])
    plt.show()


if __name__ == '__main__':
    main(sys.argv)
