#!/usr/bin/env python3
"""Polynomial regression utilising NumPy."""

import sys

from numpy import matmul, loadtxt, array, ndarray, linspace
from numpy.linalg import inv as invert
import matplotlib.pyplot as plt

import matrix


def powers(values: list, start: int, end: int) -> ndarray:
    """Convert given list of values into matrix of powers of those values."""
    return array(matrix.powers(values, start, end))


def compute_polynomial_regression(x: list, y: list, n: int = 1) -> ndarray:
    """Compute linear regression matrix for x, y."""
    x_p = powers(x, 0, n)
    y_p = powers(y, 1, 1)
    x_pt = x_p.transpose()
    return matmul(invert(matmul(x_pt, x_p)), matmul(x_pt, y_p))[:, 0]


def poly(model: ndarray, value: float) -> float:
    """Return a prediction for an outcome for a value given a model.

    >>> poly(array([[10.0], [20.0]]), 5)
    110.0
    """
    return float(sum([a*(value**n) for n, a in enumerate(model)]))


def main(argv):
    """Run regression on data from given file and print analysis."""
    data_points = loadtxt(argv[1])
    x_vals = [d[0] for d in data_points]
    x2_vals = linspace(int(min(x_vals)), int(max(x_vals)),
                       int((max(x_vals)-min(x_vals))/0.2)).tolist()
    y_vals = [d[1] for d in data_points]
    model = compute_polynomial_regression(x_vals, y_vals, int(argv[2]))

    plt.plot(x_vals, y_vals, 'ro')
    plt.plot(x2_vals, [poly(model, x) for x in x2_vals])
    plt.show()


if __name__ == '__main__':
    main(sys.argv)
