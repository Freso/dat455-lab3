"""Collection of functions to manipulate matrices."""


def transpose(matrix: list) -> list:
    """Flip rows and columns of a given matrix.

    >>> transpose([[1, 2], [3, 4], [5, 6]])
    [[1, 3, 5], [2, 4, 6]]
    """
    if not matrix:
        # Provided matrix is empty, so return as-is
        return matrix
    new_matrix = [[] for x in range(len(matrix[0]))]
    for row in matrix:
        for j, val in enumerate(row):
            new_matrix[j].append(val)
    return new_matrix


def powers(values: list, start: int, end: int) -> list:
    """Convert given list of values into matrix of powers of those values.

    Each value in the provided `values` should be turned into a row in
    the resulting matrix, with the first column being raised to the
    power of `start`, second column to value of `start`+1, etc. until
    final column being raised to power of `end`.

    >>> powers([2, 3, 4], 0, 2)
    [[1, 2, 4], [1, 3, 9], [1, 4, 16]]
    """
    matrix = [[] for x in range(len(values))]
    exponents = range(start, end+1)  # Add 1 to include `end`
    for i, value in enumerate(values):
        matrix[i] = [value**e for e in exponents]
    return matrix


def matmul(m1: list, m2: list) -> list:
    """Multiple one matrix with another.

    >>> s1 = [[0, 1], [1, 0]]
    >>> s3 = [[1, 0], [0, -1]]
    >>> matmul(s1, s3)
    [[0, -1], [1, 0]]
    >>> matmul([[1, 2, 3], [4, 5, 6]], \
               [[7, 8, 9, 10], [11, 12, 13, 14], [15, 16, 17, 18]])
    [[74, 80, 86, 92], [173, 188, 203, 218]]
    >>> I = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    >>> matmul([[1, 2, 3], [4, 5, 6], [7, 8, 9]], I)
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> matmul(I, [[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    """
    new_matrix = [[] for x in range(len(m1))]
    for i, a_row in enumerate(m1):
        for j in range(len(m2[0])):
            b_col = [b_row[j] for b_row in m2]
            k_sum = sum([a_row[n]*b_col[n] for n in range(len(a_row))])
            new_matrix[i].append(k_sum)
    return new_matrix


def invert(matrix: list) -> list:
    """Compute inverse of 2×2 matrix.

    >>> A = [[1, 2], [3, 4]]
    >>> A_inv = invert(A)
    >>> print(A_inv)
    [[-2.0, 1.0], [1.5, -0.5]]
    >>> matmul(A, A_inv)
    [[1.0, 0.0], [0.0, 1.0]]
    """
    det = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]
    return [[matrix[1][1]/det, -matrix[0][1]/det],
            [-matrix[1][0]/det, matrix[0][0]/det]]


def loadtxt(path: str) -> list:
    """Load a file and convert it to a matrix.

    >>> loadtxt("chirps.txt")
    [[12.5, 81.0], [15.27778, 97.0], [17.5, 103.0], [19.72222, 123.0], [22.2222, 150.0], [25.83333, 182.0], [28.3333, 195.0]]
    >>> loadtxt("empty.txt")
    []
    """
    with open(path) as txt:
        # Take each line and split on whitespace and convert to floats
        return [[float(i) for i in x.split()] for x in txt]
